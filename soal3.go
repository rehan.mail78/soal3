//Created by Rehan - 081295955149  *
package main

import "fmt"

func gabung(s1 string, s2 string) string {

	result := ""
	for i := 0; i < len(s1) || i < len(s2); i++ {
		if i < len(s1) {
			result += string(s1[i])
		}
		if i < len(s2) {
			result += string(s2[i])
		}
	}
	return result
}

func main() {
	fmt.Println("String 1 :")
	var str1 string
	fmt.Scanln(&str1)
	fmt.Println("String 2 :")
	var str2 string
	fmt.Scanln(&str2)
	fmt.Println("Result : ", gabung(str1, str2))

}
